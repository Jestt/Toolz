#!/bin/bash

# Requires snmpwalk installed.
# "-c public" must be changed to whatever the community id is.

ADDRESS=$1                      # address given as argument in command
OID=1.3.6.1.2.1.4.22.1.2        # OID for check
REM=iso.3.6.1.2.1.4.22.1.2.3    # Num to remove to clean output
REM2="Authoritative answers can be found from:"

echo executing on... $ADDRESS
sleep .5

com() {
    out=$(exec snmpwalk -v2c -c public $ADDRESS $OID || exec snmpwalk v1 -c pub $ADDRESS $OID) && out=${out//$REM/} && out=${out// /} 
    # command with output set as variable
    macout=$(echo "$out" | cut -f2 -d":"")
    mapfile -t macs < <(echo $macout) # reads list
    echo $ADDRESS
    for i in ${macs[*]} # loop for echoing output
    do
            :
            echo $i
    done
}

com

# Returns all connected devices on switch which responds to that OID, made for work to get devices quicker and worked well however might struggle
# in environments where devices are used which dont respond to that OID.
